import 'package:firebase_core/firebase_core.dart';
import 'package:tecno_foodie/config/provedor/carrito_compra.dart';
import 'package:tecno_foodie/config/provedor/datos_usuario.dart';
import 'package:tecno_foodie/config/provedor/id_usuario.dart';
import 'package:tecno_foodie/config/provedor/login_userdata.dart';
import 'package:tecno_foodie/config/provedor/monto_dato.dart';
import 'package:tecno_foodie/config/provedor/saldo.dart';
import 'firebase_options.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tecno_foodie/config/provedor/password_change.dart';
import 'package:tecno_foodie/config/provedor/registro_userdata.dart';
import 'package:tecno_foodie/config/router/app_router.dart';
import 'package:tecno_foodie/config/theme/app_theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
        create: (context) => PasswordChange(),
      ),
      ChangeNotifierProvider(
        create: (context) => RegistroUserdata(),
      ),
      ChangeNotifierProvider(
        create: (context) => LoginUserData(),
      ),
      ChangeNotifierProvider(
        create: (context) => CarritoCompra(),
      ),
      ChangeNotifierProvider(
        create: (context) => IdUsuario(),
      ),
      ChangeNotifierProvider(
        create: (context) => DatosUsuario(),
      ),
      ChangeNotifierProvider(
        create: (context) => Saldo(),
      ),
      ChangeNotifierProvider(
        create: (context) => MontoDato(),
      )
    ],
    child: const MainApp(),
  ));
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: appRouter,
      debugShowCheckedModeBanner: false,
      theme: AppTheme(selectedColor: 5).theme(),
    );
  }
}
