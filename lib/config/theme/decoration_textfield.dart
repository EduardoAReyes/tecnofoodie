import 'package:flutter/material.dart';

class DecorationTextfield {
  final Color colors;
  final Widget? suffixIcon;
  final String labelText;

  DecorationTextfield(
      {required this.colors,
      required this.suffixIcon,
      required this.labelText});

  InputDecoration decoration() {
    return InputDecoration(
        fillColor: colors,
        filled: true,
        suffixIcon: suffixIcon,
        labelText: labelText,
        border: OutlineInputBorder(borderSide: BorderSide(color: colors)));
  }
}
