import 'package:flutter/material.dart';

class DecorationInput {
  final Color colors;
  final TextStyle hintStyle;
  final String hintText;
  final Widget suffixIcon;

  DecorationInput(
      {required this.colors, required this.hintStyle, required this.hintText, required this.suffixIcon});

  InputDecoration decoracion() {
    return InputDecoration(
        fillColor: colors,
        filled: true,
        hintStyle: hintStyle,
        hintText: hintText,
        suffixIcon: suffixIcon);
  }
}