import 'package:flutter/material.dart';

const List<Color> _colorThemes = [
  Colors.purple,
  Colors.pink,
  Colors.blue,
  Colors.orange,
  Colors.yellow,
  Colors.brown
];

class AppTheme {
  final int selectedColor;

  AppTheme({required this.selectedColor})
      : assert(selectedColor >= 0),
        assert(selectedColor < _colorThemes.length);
  ThemeData theme() {
    return ThemeData(colorSchemeSeed: _colorThemes[selectedColor],);
  }
}
