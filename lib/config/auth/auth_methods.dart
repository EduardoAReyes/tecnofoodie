import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AuthMethods {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final db = FirebaseFirestore.instance;

  Future<String> registrarUsuario(
      {required String usuario,
      required String nombre,
      required String carrera,
      required String email,
      required String password}) async {
    String resp = "";
    try {
      if (nombre.isNotEmpty ||
          email.isNotEmpty ||
          password.isNotEmpty ||
          usuario.isNotEmpty ||
          carrera.isNotEmpty) {
        await _auth.createUserWithEmailAndPassword(
            email: email, password: password);

        await db.collection("users").doc(email).set({
          "Nombre": nombre,
          "Usuario": usuario,
          "Carrera": carrera,
          "Email": email
        });
        resp = 'Success';
      }
    } catch (e) {
      resp = e.toString();
    }
    return resp;
  }

  Future<String> iniciarUsuario(
      {required String usuario, required String pass}) async {
    String resp = "";
    try {
      if (usuario.isNotEmpty || pass.isNotEmpty) {
        await _auth.signInWithEmailAndPassword(email: usuario, password: pass);
        resp = 'Success';
      }
    } catch (e) {
      resp = e.toString();
    }
    return resp;
  }

  Future<String> logOut() async {
    String resp = "";
    try {
      await _auth.signOut();
      resp = 'logout';
    } catch (e) {
      resp = e.toString();
    }
    return resp;
  }
}
