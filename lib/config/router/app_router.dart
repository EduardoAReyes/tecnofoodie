import 'package:go_router/go_router.dart';
import 'package:tecno_foodie/presentation/screens/carrito/carrito_screen.dart';
import 'package:tecno_foodie/presentation/screens/home/home_screen.dart';
import 'package:tecno_foodie/presentation/screens/login/form/registro/custom_form_registro.dart';
import 'package:tecno_foodie/presentation/screens/menu/menu_screen.dart';
import 'package:tecno_foodie/presentation/screens/tarjeta/monto_screen.dart';
import 'package:tecno_foodie/presentation/screens/tarjeta/tarjeta_screen.dart';
import 'package:tecno_foodie/presentation/screens/usuario/usuario_screen.dart';

// GoRouter configuration
final appRouter = GoRouter(
  initialLocation: '/',
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const HomeScreen(),
    ),
    GoRoute(
      path: '/registrarForm',
      builder: (context, state) => const CustomFormRegistro(),
    ),
    GoRoute(
      path: '/menu',
      builder: (context, state) => const MenuScreen(),
    ),
    
    GoRoute(
      path: '/Pedidos',
      builder: (context, state) => const CarritoScreen(),
    ),
    GoRoute(
      path: '/Perfil',
      builder: (context, state) => const UsuarioScreen(),
    ),
    GoRoute(
      path: '/tarjeta',
      builder: (context, state) => const MontoScreen(),
    ),
  ],
);
