import 'package:flutter/material.dart';

class PasswordChange with ChangeNotifier {
  bool _eyeOpen = true;

  bool get eyeOpen => _eyeOpen;

  void obscureText() {
    _eyeOpen = false;
    notifyListeners();
  }

  void showText() {
    _eyeOpen = true;
    notifyListeners();
  }
}
