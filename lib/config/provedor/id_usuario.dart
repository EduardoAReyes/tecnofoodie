import 'package:flutter/material.dart';

class IdUsuario with ChangeNotifier {
  String _idDocumento = '';

  String get idDocumento => _idDocumento;

  void capturarIdDocumento(String idDocumento) {
    _idDocumento = idDocumento;
    notifyListeners();
  }
}
