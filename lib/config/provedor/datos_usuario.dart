import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class DatosUsuario with ChangeNotifier {
  final db = FirebaseFirestore.instance;
  final List<dynamic> _datos = [];

  List<dynamic> get datos => _datos;

  Future<void> datosUsuario(String idUsuario) async {
    final docRef = db.collection('users').doc(idUsuario);

    final docSnapshot = await docRef.get();

    if (docSnapshot.exists) {
      final datos = docSnapshot.data();
      _datos.add(datos);
      notifyListeners();
    }else{
    print('hubo un error');
    }
  }
}
