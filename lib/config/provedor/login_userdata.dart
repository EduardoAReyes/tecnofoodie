import 'package:flutter/material.dart';

class LoginUserData with ChangeNotifier {
  String _usuario = '';
  String _password = '';

  String get usuario => _usuario;
  String get password => _password;

  void capturaUsuario(String usuario) {
    _usuario = usuario;
    notifyListeners();
  }

  void capturaPassword(String password) {
    _password = password;
    notifyListeners();
  }
  
}
