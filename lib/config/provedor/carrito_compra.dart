import 'package:flutter/material.dart';
import 'package:tecno_foodie/presentation/screens/carrito/carrito_item.dart';

class CarritoCompra with ChangeNotifier {
  final List<dynamic> _carrito = [];
  int _total = 0;
  String _botonAgregar = 'Agregar';
  bool _habilitadoBotonAgregar = true;

  List<dynamic> get carrito => _carrito;
  int get total => _total;
  String get botonAgregar => _botonAgregar;
  bool get habilitadoBotonAgregar => _habilitadoBotonAgregar;

  void agregarCarrito(CarritoItem carritoItem) {
    carrito.add(carritoItem);
    notifyListeners();
  }

  void removerCarrito(int index) {
    carrito.removeAt(index);
    notifyListeners();
  }

  void acumularTotal(int precio) {
    _total += precio;
    notifyListeners();
  }

  void descontarTotal(int precio) {
    _total -= precio;
    notifyListeners();
  }

  void vaciarCarrito() {
    _total = 0;
    carrito.clear();
    notifyListeners();
  }
  
}
