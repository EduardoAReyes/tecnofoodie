import 'package:flutter/material.dart';

class RegistroUserdata with ChangeNotifier {
  String _nombre = '';
  String _usuario = '';
  String _email = '';
  String _password = '';
  String _carrera = '';
  

  String get nombre => _nombre;
  String get email => _email;
  String get password => _password;
  String get usuario => _usuario;
  String get carrera => _carrera;


  void capturarNombre(String nombre) {
    _nombre = nombre;
    notifyListeners();
  }

  void capturarUsuario(String usuario) {
    _usuario = usuario;
    notifyListeners();
  }

  void capturarCarrera(String carrera) {
    _carrera = carrera;
    notifyListeners();
  }

  void capturarEmail(String email) {
    _email = email;
    notifyListeners();
  }

  void capturarPassword(String password) {
    _password = password;
    notifyListeners();
  }
}
