import 'package:flutter/material.dart';

class Saldo with ChangeNotifier {
  int _saldo = 0;

  int get saldo => _saldo;

  void cargarSaldo(int saldo) {
    _saldo = saldo;
    notifyListeners();
  }

  void descontarSaldo(int saldo) {
    _saldo -= saldo;
    if (_saldo < 0) {
      _saldo = 0;
    }
    notifyListeners();
  }
}
