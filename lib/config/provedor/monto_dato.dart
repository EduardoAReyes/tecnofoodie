import 'package:flutter/material.dart';

class MontoDato with ChangeNotifier {
  String _nTarjeta = '';
  String _nTitular = '';
  String _fechaExp = '';
  String _cvv = '';
  String _monto = '';

  String get nTarjeta => _nTarjeta;
  String get nTitular => _nTitular;
  String get fechaExp => _fechaExp;
  String get cvv => _cvv;
  String get monto => _monto;

  void capturarMonto(String monto){
    _monto=monto;
    notifyListeners();
  }

}
