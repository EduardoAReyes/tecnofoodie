import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tecno_foodie/config/provedor/saldo.dart';
class Cuenta extends StatelessWidget {
  const Cuenta({super.key});

  @override
  Widget build(BuildContext context) {
    final saldo = Provider.of<Saldo>(context);
    final colors = Theme.of(context).colorScheme;
    return Center(
        child: Text(
      'Saldo: ${saldo.saldo}',
      style: TextStyle(fontSize: 25,color:colors.primary),
    ));
  }
  
}
