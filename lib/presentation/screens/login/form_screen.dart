import 'package:flutter/material.dart';
import 'package:tecno_foodie/presentation/screens/login/form/login/my_custom_form.dart';

class FormScreen extends StatelessWidget {
  const FormScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: SingleChildScrollView(
                child: Column(
          children: [
            Stack(
              children: [
                SizedBox(
                  height: 300,
                  child: Image.asset('assets/AGUILA.png'),
                )
              ],
            ),
            const MyCustomForm(),
          ],
                ),
              ),
        ));
  }
}
