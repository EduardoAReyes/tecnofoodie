import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:tecno_foodie/config/auth/auth_methods.dart';
import 'package:tecno_foodie/config/provedor/datos_usuario.dart';
import 'package:tecno_foodie/config/provedor/id_usuario.dart';
import 'package:tecno_foodie/config/provedor/login_userdata.dart';
import 'package:tecno_foodie/config/provedor/password_change.dart';
import 'package:tecno_foodie/config/theme/decoration_input.dart';

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({super.key});

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;
    final passwordChange = Provider.of<PasswordChange>(context);
    final loginData = Provider.of<LoginUserData>(context, listen: false);
    final datosUsuario = Provider.of<IdUsuario>(context, listen: false);
    final idUsuario = Provider.of<IdUsuario>(context);
    final usuario = Provider.of<DatosUsuario>(context, listen: false);

    void menu() {
      usuario.datosUsuario(idUsuario.idDocumento);
      FocusScope.of(context).unfocus();
      context.go('/menu');
    }



    void loginUsuario() async {
      String resp = await AuthMethods()
          .iniciarUsuario(usuario: loginData.usuario, pass: loginData.password);
      if (resp == 'Success') {
        datosUsuario.capturarIdDocumento(loginData.usuario);
        menu();
      } else if (resp == "") {
        Fluttertoast.showToast(
            msg: "Campos vacios",
            toastLength: Toast.LENGTH_SHORT,
            timeInSecForIosWeb: 1,
            backgroundColor: const Color.fromARGB(255, 207, 0, 0),
            textColor: Colors.white,
            fontSize: 16.0);
      } else {
        Fluttertoast.showToast(
            msg: "Error en credenciales",
            toastLength: Toast.LENGTH_SHORT,
            timeInSecForIosWeb: 1,
            backgroundColor: const Color.fromARGB(255, 207, 0, 0),
            textColor: Colors.white,
            fontSize: 16.0);
      }
    }

    return FormBuilder(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50),
          child: Column(
            children: <Widget>[
              const Text(
                'Iniciar Sesión',
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              FormBuilderTextField(
                name: 'Email',
                keyboardType: TextInputType.emailAddress,
                decoration: DecorationInput(
                  colors: colors.primary.withOpacity(0.1),
                  hintStyle: TextStyle(color: colors.primary.withOpacity(0.3)),
                  hintText: 'Email',
                  suffixIcon: const Icon(Icons.alternate_email),
                ).decoracion(),
                onChanged: (value) {
                  loginData.capturaUsuario(value.toString());
                },
              ),
              const SizedBox(height: 20),
              FormBuilderTextField(
                name: 'password',
                decoration: DecorationInput(
                  colors: colors.primary.withOpacity(0.1),
                  hintStyle: TextStyle(color: colors.primary.withOpacity(0.3)),
                  hintText: 'Contraseña',
                  suffixIcon: passwordChange.eyeOpen
                      ? IconButton(
                          onPressed: () {
                            passwordChange.obscureText();
                          },
                          icon: const Icon(Icons.visibility_off),
                        )
                      : IconButton(
                          onPressed: () {
                            passwordChange.showText();
                          },
                          icon: const Icon(Icons.visibility),
                        ),
                ).decoracion(),
                obscureText: passwordChange.eyeOpen,
                onChanged: (value) {
                  loginData.capturaPassword(value.toString());
                },
              ),
              const SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    '¿No tienes cuenta?',
                    style: TextStyle(color: Colors.black.withOpacity(0.3)),
                  ),
                  GestureDetector(
                    onTap: () => context.go('/registrarForm'),
                    child: const Text(
                      'Crear cuenta',
                      style: TextStyle(color: Color.fromARGB(255, 8, 0, 119)),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                onPressed: () {
                  loginUsuario();
                },
                child: const Text('Entrar'),
              ),
            ],
          ),
        ));
  }
}
