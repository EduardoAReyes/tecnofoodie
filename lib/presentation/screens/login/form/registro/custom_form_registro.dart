import 'package:flutter/material.dart';
import 'package:tecno_foodie/presentation/screens/login/form/registro/registrar_form.dart';

class CustomFormRegistro extends StatelessWidget {
  const CustomFormRegistro({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                SizedBox(
                  height: 200,
                  child: Image.asset('assets/AGUILA.png'),
                ),
              ],
            ),
                RegistrarForm()
          ],
        ),
      ),
    ));
  }
}
