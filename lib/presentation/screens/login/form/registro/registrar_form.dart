import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:tecno_foodie/config/auth/auth_methods.dart';
import 'package:tecno_foodie/config/provedor/password_change.dart';
import 'package:tecno_foodie/config/provedor/registro_userdata.dart';
import 'package:tecno_foodie/config/theme/decoration_input.dart';

class RegistrarForm extends StatelessWidget {
  RegistrarForm({super.key});
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;
    final passwordChange = Provider.of<PasswordChange>(context);
    final registroUserdata =
        Provider.of<RegistroUserdata>(context, listen: false);

    void home() {
      context.go('/');
    }

    void registrarUsuario() async {
      String resp = await AuthMethods().registrarUsuario(
          usuario: registroUserdata.usuario,
          nombre: registroUserdata.nombre,
          carrera: registroUserdata.carrera,
          email: registroUserdata.email,
          password: registroUserdata.password);

      if (resp == 'Success') {
        home();
      }
    }

    return FormBuilder(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50),
          child: Column(
            children: <Widget>[
              const Text(
                'Crear cuenta',
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              FormBuilderTextField(
                name: 'Usuario',
                keyboardType: TextInputType.text,
                decoration: DecorationInput(
                  colors: colors.primary.withOpacity(0.1),
                  hintStyle: TextStyle(color: colors.primary.withOpacity(0.3)),
                  hintText: 'Usuario',
                  suffixIcon: const Icon(Icons.person),
                ).decoracion(),
                onChanged: (value) {
                  registroUserdata.capturarUsuario(value.toString());
                },
              ),
              const SizedBox(
                height: 20,
              ),
              FormBuilderTextField(
                name: 'Nombre',
                keyboardType: TextInputType.text,
                decoration: DecorationInput(
                  colors: colors.primary.withOpacity(0.1),
                  hintStyle: TextStyle(color: colors.primary.withOpacity(0.3)),
                  hintText: 'Nombre',
                  suffixIcon: const Icon(Icons.person),
                ).decoracion(),
                onChanged: (value) {
                  registroUserdata.capturarNombre(value.toString());
                },
              ),
              
              const SizedBox(
                height: 20,
              ),
              FormBuilderTextField(
                name: 'Carrera',
                keyboardType: TextInputType.text,
                decoration: DecorationInput(
                  colors: colors.primary.withOpacity(0.1),
                  hintStyle: TextStyle(color: colors.primary.withOpacity(0.3)),
                  hintText: 'Carrera',
                  suffixIcon: const Icon(Icons.person),
                ).decoracion(),
                onChanged: (value) {
                  registroUserdata.capturarCarrera(value.toString());
                },
              ),
              const SizedBox(
                height: 20,
              ),
              FormBuilderTextField(
                name: 'Email',
                keyboardType: TextInputType.emailAddress,
                decoration: DecorationInput(
                  colors: colors.primary.withOpacity(0.1),
                  hintStyle: TextStyle(color: colors.primary.withOpacity(0.3)),
                  hintText: 'Email',
                  suffixIcon: const Icon(Icons.alternate_email),
                ).decoracion(),
                onChanged: (value) {
                  registroUserdata.capturarEmail(value.toString());
                },
              ),
              const SizedBox(height: 20),
              FormBuilderTextField(
                name: 'password',
                decoration: DecorationInput(
                  colors: colors.primary.withOpacity(0.1),
                  hintStyle: TextStyle(color: colors.primary.withOpacity(0.3)),
                  hintText: 'Contraseña',
                  suffixIcon: passwordChange.eyeOpen
                      ? IconButton(
                          onPressed: () {
                            passwordChange.obscureText();
                          },
                          icon: const Icon(Icons.visibility_off),
                        )
                      : IconButton(
                          onPressed: () {
                            passwordChange.showText();
                          },
                          icon: const Icon(Icons.visibility),
                        ),
                ).decoracion(),
                obscureText: passwordChange.eyeOpen,
                onChanged: (value) {
                  registroUserdata.capturarPassword(value.toString());
                },
              ),
              const SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    '¿Ya tienes cuenta?',
                    style: TextStyle(color: Colors.black.withOpacity(0.3)),
                  ),
                  GestureDetector(
                    onTap: () => context.go('/'),
                    child: const Text(
                      'Iniciar Sesión',
                      style: TextStyle(color: Color.fromARGB(255, 8, 0, 119)),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                onPressed: () {
                  registrarUsuario();
                },
                child: const Text('Crear'),
              )
            ],
          ),
        ));
  }
}
