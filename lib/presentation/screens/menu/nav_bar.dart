import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:tecno_foodie/config/auth/auth_methods.dart';

class NavBar extends StatelessWidget {
  const NavBar({super.key});

  @override
  Widget build(BuildContext context) {

    final colors = Theme.of(context).colorScheme;
    void home() {
      context.go('/');
    }

    void logout() async {
      String resp = await AuthMethods().logOut();
      if (resp == 'logout') {
        home();
      }
    }

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          SizedBox(
            height: 130,
            child: DrawerHeader(
              decoration: BoxDecoration(
                color: colors.primary,
              ),
              child: const Text(
                'TecnoFoodie',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
              ),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.home),
            title: const Text('Inicio'),
            onTap: () {
              context.go('/Menu');
            },
          ),
          ListTile(
            leading: const Icon(Icons.person),
            title: const Text('Perfil'),
            onTap: () {
              context.go('/Perfil');
            },
          ),

          ListTile(
            leading: const Icon(Icons.shopping_cart),
            title: const Text('Pedidos'),
            onTap: () {
              context.go('/Pedidos');
            },
          ),
          ListTile(
            leading: const Icon(Icons.payment),
            title: const Text('Agregar tarjeta'),
            onTap: () {
              // context.go('/Perfil');
              context.go('/tarjeta');
            },
          ),

          ListTile(
            leading: const Icon(Icons.logout),
            title: const Text('Cerrar Sesión'),
            onTap: () {
              // Navegar a la pantalla de configuración
              logout();
              // Aquí puedes agregar la lógica para navegar a la pantalla de configuración
            },
          ),

          // Agrega más ListTile para más opciones de menú
        ],
      ),
    );
  }
}
