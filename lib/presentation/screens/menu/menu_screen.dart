import 'package:flutter/material.dart';
import 'package:tecno_foodie/presentation/screens/cuenta/seccion_cuenta.dart';
import 'package:tecno_foodie/presentation/screens/menu/nav_bar.dart';
import 'package:tecno_foodie/presentation/screens/productos/seccion_productos.dart';

class MenuScreen extends StatelessWidget {
  const MenuScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.grey,
        scrolledUnderElevation: 20.0,
      ),
      drawer: const NavBar(), // Llama a la función para obtener el drawer
      body: const SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: [
              SeccionProductos(),
              SizedBox(
                height: 20,
              ),
              SeccionCuenta()
            ],
          ),
        ),
      ),
    );
  }
}
