import 'package:flutter/material.dart';
import 'package:tecno_foodie/presentation/screens/menu/nav_bar.dart';
import 'package:tecno_foodie/presentation/screens/tarjeta/seccion_tarjeta.dart';

class TarjetaScreen extends StatelessWidget {
  const TarjetaScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.grey,
        scrolledUnderElevation: 20.0,
      ),
      drawer: const NavBar(),
      body: const SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: SeccionTarjeta(),
        ),
      ),
    );
  }
}
