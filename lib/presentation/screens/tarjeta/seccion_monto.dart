import 'package:flutter/material.dart';
import 'package:tecno_foodie/presentation/screens/tarjeta/monto.dart';

class SeccionMonto extends StatelessWidget {
  const SeccionMonto({super.key});
  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;
    final size = MediaQuery.of(context).size;

    return Center(
      child: Container(
        width: size.width * 0.8,
        height: size.height * 0.6,
        decoration: BoxDecoration(border: Border.all(color: colors.primary)),
        child: Center(
            child: Column(
          children: [
            Container(
                margin: EdgeInsets.only(right: size.width * 0.52),
                child: Text(
                  'Monto',
                  style: TextStyle(
                      color: colors.primary,
                      fontSize: 25,
                      fontWeight: FontWeight.w500),
                )),
            Container(
              width: size.width * 0.7,
              height: size.height * 0.52,
              decoration:
                  BoxDecoration(border: Border.all(color: colors.primary)),
              child: const Monto(),
            ),
          ],
        )),
      ),
    );
  }
}
