import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:tecno_foodie/config/provedor/monto_dato.dart';
import 'package:tecno_foodie/config/provedor/saldo.dart';
import 'package:tecno_foodie/config/theme/decoration_input.dart';
class Monto extends StatefulWidget {
  const Monto({super.key});

  @override
  State<Monto> createState() => _MontoState();
}

class _MontoState extends State<Monto> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;
    final montoDato = Provider.of<MontoDato>(context);
    final saldo = Provider.of<Saldo>(context);

    return FormBuilder(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FormBuilderTextField(
                inputFormatters: [
                  LengthLimitingTextInputFormatter(19),
                  FilteringTextInputFormatter.digitsOnly,
                  CreditCardInputFormat()
                ],
                name: 'Número de tarjeta',
                keyboardType: TextInputType.number,
                decoration: DecorationInput(
                  colors: colors.primary.withOpacity(0.1),
                  hintStyle: TextStyle(color: colors.primary.withOpacity(0.3)),
                  hintText: 'Número de tarjeta',
                  suffixIcon: const Icon(Icons.credit_card_outlined),
                ).decoracion(),
                onChanged: (value) {},
              ),
              const SizedBox(
                height: 12,
              ),
              FormBuilderTextField(
                name: 'Nombre del titular',
                keyboardType: TextInputType.name,
                decoration: DecorationInput(
                  colors: colors.primary.withOpacity(0.1),
                  hintStyle: TextStyle(color: colors.primary.withOpacity(0.3)),
                  hintText: 'Nombre del titular',
                  suffixIcon: const Icon(Icons.person),
                ).decoracion(),
                onChanged: (value) {},
              ),
              const SizedBox(
                height: 12,
              ),
              FormBuilderTextField(
                inputFormatters: [
                  LengthLimitingTextInputFormatter(5),
                  FilteringTextInputFormatter.digitsOnly,
                  DateInputFormat()
                ],
                name: 'Fecha de expiración',
                keyboardType: TextInputType.text,
                decoration: DecorationInput(
                  colors: colors.primary.withOpacity(0.1),
                  hintStyle: TextStyle(color: colors.primary.withOpacity(0.3)),
                  hintText: 'Fecha de expiración',
                  suffixIcon: const Icon(Icons.calendar_month),
                ).decoracion(),
                onChanged: (value) {},
              ),
              const SizedBox(
                height: 12,
              ),
              FormBuilderTextField(
                name: 'CVV / CVC',
                keyboardType: TextInputType.number,
                decoration: DecorationInput(
                  colors: colors.primary.withOpacity(0.1),
                  hintStyle: TextStyle(color: colors.primary.withOpacity(0.3)),
                  hintText: 'CVV / CVC',
                  suffixIcon: const Icon(Icons.lock),
                ).decoracion(),
                onChanged: (value) {},
              ),
              const SizedBox(
                height: 12,
              ),
              FormBuilderTextField(
                inputFormatters: [
                  LengthLimitingTextInputFormatter(5),
                  FilteringTextInputFormatter.digitsOnly,
                ],
                name: 'Monto',
                keyboardType: TextInputType.number,
                decoration: DecorationInput(
                  colors: colors.primary.withOpacity(0.1),
                  hintStyle: TextStyle(color: colors.primary.withOpacity(0.3)),
                  hintText: 'Monto',
                  suffixIcon: const Icon(Icons.attach_money_sharp),
                ).decoracion(),
                onChanged: (value) {
                  montoDato.capturarMonto(value.toString());
                },
              ),
              const SizedBox(
                height: 40,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                    onPressed: () {
                      int montoInt = int.parse(montoDato.monto);
                      saldo.cargarSaldo(montoInt);
                      context.go('/Menu');
                    },
                    child: const Text('Cargar')),
              )
            ],
          ),
        ));
  }
}

class DateInputFormat extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    String enteredData = newValue.text;
    StringBuffer buffer = StringBuffer();

    for (int i = 0; i < enteredData.length; i++) {
      buffer.write(enteredData[i]);
      int index = i + 1;
      if (index % 2 == 0 && enteredData.length != index) {
        buffer.write('/');
      }
    }

    return TextEditingValue(
        text: buffer.toString(),
        selection: TextSelection.collapsed(offset: buffer.toString().length));
  }
}

class CreditCardInputFormat extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    String enteredData = newValue.text;
    StringBuffer buffer = StringBuffer();

    for (int i = 0; i < enteredData.length; i++) {
      buffer.write(enteredData[i]);
      int index = i + 1;
      if (index % 4 == 0 && enteredData.length != index) {
        buffer.write(' ');
      }
    }

    return TextEditingValue(
        text: buffer.toString(),
        selection: TextSelection.collapsed(offset: buffer.toString().length));
  }
}
