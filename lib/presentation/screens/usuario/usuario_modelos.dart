import 'package:flutter/material.dart';

class UsuarioModelos{
  final String nombre;
  final String usuario;
  final String carrera;
  final String email;

  UsuarioModelos({required this.nombre, required this.usuario, required this.carrera, required this.email});
  
}