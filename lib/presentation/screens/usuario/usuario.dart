import 'package:flutter/material.dart';
import 'package:tecno_foodie/config/provedor/datos_usuario.dart';
import 'package:tecno_foodie/config/theme/decoration_textField.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

class Usuario extends StatefulWidget {
  const Usuario({super.key});

  @override
  State<Usuario> createState() => _UsuarioState();
}

class _UsuarioState extends State<Usuario> {
  @override
  Widget build(BuildContext context) {
    final datosUsuario = Provider.of<DatosUsuario>(context);
    final colors = Theme.of(context).colorScheme;
    return Column(
      children: [
        SizedBox(
          height: 60,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              enabled: false,
              decoration: DecorationTextfield(
                      colors: colors.primary.withOpacity(0.1),
                      suffixIcon: const Icon(Icons.person),
                      labelText: datosUsuario.datos[0]['Nombre'])
                  .decoration(),
            ),
          ),
        ),
        SizedBox(
          height: 60,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              enabled: false,
              decoration: DecorationTextfield(
                      colors: colors.primary.withOpacity(0.1),
                      suffixIcon: const Icon(Icons.person_outline_outlined),
                      labelText: datosUsuario.datos[0]['Usuario'])
                  .decoration(),
            ),
          ),
        ),
        SizedBox(
          height: 60,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              enabled: false,
              decoration: DecorationTextfield(
                      colors: colors.primary.withOpacity(0.1),
                      suffixIcon: const Icon(Icons.school),
                      labelText:  datosUsuario.datos[0]['Carrera'])
                  .decoration(),
            ),
          ),
        ),
        SizedBox(
          height: 60,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              enabled: false,
              decoration: DecorationTextfield(
                      colors: colors.primary.withOpacity(0.1),
                      suffixIcon: const Icon(Icons.alternate_email),
                      labelText: datosUsuario.datos[0]['Email'])
                  .decoration(),
            ),
          ),
        ),
        ElevatedButton(
            onPressed: () {
              context.go('/Menu');
            },
            child: const Text('Cerrar'))
      ],
    );
  }
}
