import 'package:flutter/material.dart';
import 'package:tecno_foodie/presentation/screens/login/form_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body:FormScreen()
    );
  }
}
