import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tecno_foodie/config/provedor/carrito_compra.dart';
import 'package:tecno_foodie/presentation/screens/carrito/carrito_item.dart';
import 'package:tecno_foodie/presentation/screens/productos/product_items.dart';
import 'package:provider/provider.dart';

class ListviewProductos extends StatelessWidget {
  const ListviewProductos({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: appProductItems.length,
      itemBuilder: (context, index) {
        final productItems = appProductItems[index];
        return _CustomListMenu(productItems: productItems);
      },
    );
  }
}

class _CustomListMenu extends StatelessWidget {
  final ProductItems productItems;

  const _CustomListMenu({required this.productItems});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final colors = Theme.of(context).colorScheme;
    final carritoCompra = Provider.of<CarritoCompra>(context);
    final habilitado = carritoCompra.habilitadoBotonAgregar;

    return Container(
        margin: const EdgeInsets.only(bottom: 15),
        width: size.width * 0.8,
        height: size.height * 0.2,
        decoration: BoxDecoration(color: colors.primary.withOpacity(0.2)),
        padding: const EdgeInsets.all(2),
        child: Card(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                padding: const EdgeInsets.all(5),
                child: Image.asset('assets/${productItems.link}'),
              ),
              Expanded(
                  child: Container(
                padding: const EdgeInsets.all(5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(
                      productItems.nombre,
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text('Precio: ${productItems.precio}'),
                    ElevatedButton(
                      onPressed: habilitado
                          ? () {
                              //$ Agregar aqui que agregue las propiedades al carrito de compra
                              int precioInt = int.parse(productItems.precio);
                              carritoCompra.agregarCarrito(CarritoItem(
                                  nombre: productItems.nombre,
                                  precio: precioInt,
                                  link: productItems.link));
                              carritoCompra.acumularTotal(precioInt);


                              Fluttertoast.showToast(
                                  msg: "Se agregó",
                                  toastLength: Toast.LENGTH_SHORT,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor:
                                      const Color.fromARGB(255, 13, 156, 0),
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            }
                          : null,
                      child: Text(carritoCompra.botonAgregar),
                    )
                  ],
                ),
              ))
            ],
          ),
        ));
  }
}
