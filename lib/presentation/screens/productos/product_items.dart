import 'package:flutter/material.dart';

class ProductItems {
  final String nombre;
  final String precio;
  final String link;

  const ProductItems(
      {required this.nombre, required this.precio, required this.link});
}

const appProductItems = <ProductItems>[
  ProductItems(nombre: 'Tacos al pastor', precio: '40', link: 'TACOS.png'),
  ProductItems(
      nombre: 'Tacos de barbacoa', precio: '46', link: 'TACOS_BARBA.png'),
  ProductItems(
      nombre: 'Tacos de longaniza', precio: '35', link: 'TACOS_LONGA.png'),
  ProductItems(nombre: 'Tacos de suadero', precio: '45', link: 'TACOS_SUADERO.png'),
  ProductItems(nombre: 'Boing de mango', precio: '15', link: 'BOING_MANGO.png'),
  ProductItems(nombre: 'Boing de uva', precio: '15', link: 'BOING_UVA.png'),
  ProductItems(nombre: 'Boing de guayaba', precio: '15', link: 'BOING_GUA.png'),
];
