import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tecno_foodie/config/provedor/carrito_compra.dart';
import 'package:provider/provider.dart';

class ListviewCarrito extends StatelessWidget {
  const ListviewCarrito({super.key});

  @override
  Widget build(BuildContext context) {
    final carritoCompra = Provider.of<CarritoCompra>(context);
    return ListView.builder(
      itemCount: carritoCompra.carrito.length,
      itemBuilder: (context, index) {
        final carritoItems = carritoCompra.carrito[index];
        return _CustomListCarrito(carritoItems: carritoItems, index: index);
      },
    );
  }
}

class _CustomListCarrito extends StatelessWidget {
  final dynamic carritoItems;
  final int index;

  const _CustomListCarrito({required this.carritoItems, required this.index});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final colors = Theme.of(context).colorScheme;
    final carritoCompra = Provider.of<CarritoCompra>(context);

    return Container(
      margin: const EdgeInsets.only(bottom: 15),
      width: size.width * 0.8,
      height: size.height * 0.2,
      decoration: BoxDecoration(color: colors.primary.withOpacity(0.2)),
      padding: const EdgeInsets.all(2),
      child: Card(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              padding: const EdgeInsets.all(5),
              child: Image.asset('assets/${carritoItems.link}'),
            ),
            Expanded(
                child: Container(
              padding: const EdgeInsets.all(5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    carritoItems.nombre,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text('Precio: ${carritoItems.precio}'),
                  ElevatedButton(
                      onPressed: () {
                        carritoCompra.removerCarrito(index);
                        carritoCompra.descontarTotal(carritoItems.precio);
                      },
                      child: const Text('Eliminar'))
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
