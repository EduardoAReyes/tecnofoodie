import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:go_router/go_router.dart';
import 'package:tecno_foodie/config/provedor/carrito_compra.dart';
import 'package:tecno_foodie/config/provedor/saldo.dart';
import 'package:tecno_foodie/presentation/screens/carrito/listview_carrito.dart';
import 'package:provider/provider.dart';

class SeccionCarrito extends StatelessWidget {
  const SeccionCarrito({super.key});
  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;
    final size = MediaQuery.of(context).size;
    final carritoCompra = Provider.of<CarritoCompra>(context);
    final saldo = Provider.of<Saldo>(context);
    return Center(
      child: Container(
        width: size.width * 0.8,
        height: size.height * 0.7,
        decoration: BoxDecoration(border: Border.all(color: colors.primary)),
        child: Center(
            child: Column(
          children: [
            Container(
                margin: EdgeInsets.only(right: size.width * 0.47),
                child: Text(
                  'Pedidos',
                  style: TextStyle(
                      color: colors.primary,
                      fontSize: 25,
                      fontWeight: FontWeight.w500),
                )),
            Container(
              width: size.width * 0.7,
              height: size.height * 0.52,
              decoration:
                  BoxDecoration(border: Border.all(color: colors.primary)),
              child: const ListviewCarrito(),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                children: [
                  Text('Total a pagar: ${carritoCompra.total}'),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                        onPressed: () {
                          if (saldo.saldo >= carritoCompra.total &&
                              saldo.saldo != 0) {
                            saldo.descontarSaldo(carritoCompra.total);
                            carritoCompra.vaciarCarrito();
                            context.go('/Menu');

                            Fluttertoast.showToast(
                                msg: "Compra realizada!",
                                toastLength: Toast.LENGTH_SHORT,
                                timeInSecForIosWeb: 1,
                                backgroundColor:
                                    Color.fromARGB(255, 13, 156, 0),
                                textColor: Colors.white,
                                fontSize: 16.0);
                          } else {
                            Fluttertoast.showToast(
                                msg: "Saldo insuficiente",
                                toastLength: Toast.LENGTH_SHORT,
                                timeInSecForIosWeb: 1,
                                backgroundColor:
                                    const Color.fromARGB(255, 207, 0, 0),
                                textColor: Colors.white,
                                fontSize: 16.0);
                          }
                        },
                        child: const Text('Realizar compra')),
                  )
                ],
              ),
            ),
          ],
        )),
      ),
    );
  }
}
