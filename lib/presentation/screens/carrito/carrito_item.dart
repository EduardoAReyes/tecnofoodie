
class CarritoItem{
  final String nombre;
  final int precio;
  final String link;

  CarritoItem({required this.nombre, required this.precio, required this.link});
}

